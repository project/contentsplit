<?php

namespace Drupal\content_split\Form;


use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Class CustomSettingsForm.
 */
class CustomSettingsForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'custom_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        // Get the form values and raw input (unvalidated values).
        $values = $form_state->getValues();

        // Define a wrapper id to populate new content into.
        $ajax_wrapper = 'my-ajax-wrapper';

        $node_types = NodeType::loadMultiple();
        // If you need to display them in a drop down:
        $sourceOptions = [];
        foreach ($node_types as $node_type) {
            $sourceOptions[$node_type->id()] = $node_type->label();
        }
        $form['source_content_type'] = [
            '#type' => 'select',
            '#title' => $this->t('Source content type'),
            '#description' => $this->t('Select source content type. If the exact number and same name fields are present in any other content type then that will be displayed.'),
            '#required' => TRUE,
            '#options' => $sourceOptions,
            '#ajax' => [
                'callback' => [$this, 'mySelectChange'],
                'event' => 'change',
                'wrapper' => $ajax_wrapper,
            ],
        ];

        // Build a wrapper for the ajax response.
        $form['my_ajax_container'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => $ajax_wrapper,
            ]
        ];

        // ONLY LOADED IN AJAX RESPONSE OR IF FORM STATE VALUES POPULATED.
        if (!empty($values) && !empty($values['source_content_type'])) {
            //Remove the source content type from the destination options
            unset($sourceOptions[$values['source_content_type']]);
            //First we will validate if any content type has same number and type of fields available.
            $entityManager = \Drupal::service('entity.manager');
            $fields = array_filter(
                $entityManager->getFieldDefinitions('node', $values['source_content_type']),
                function ($field_definition) {
                    return
                        $field_definition instanceof FieldConfigInterface;
                }
            );
            $sourceTaxFilterValidation = [];
            foreach ($fields as  $field){
                array_push($sourceTaxFilterValidation, $field->get('field_name'));
            }

            foreach($sourceOptions as $destinationOption => $destinationOptionValue){
                $entityManager = \Drupal::service('entity.manager');
                $fields = array_filter(
                    $entityManager->getFieldDefinitions('node', $destinationOption),
                    function ($field_definition) {
                        return
                            $field_definition instanceof FieldConfigInterface;
                    }
                );
                if(count($fields) == count($sourceTaxFilterValidation)){
                    foreach ($fields as  $field){
                        if(!in_array($field->get('field_name'), $sourceTaxFilterValidation)){
                            unset($sourceOptions[$destinationOption]);
                        }
                    }
                }
                else{
                    unset($sourceOptions[$destinationOption]);
                }
            }
            //Validation ends here
            if(!empty($sourceOptions)){
                $form['my_ajax_container']['destination_content_type'] = [
                    '#type' => 'select',
                    '#title' => $this->t('Destination content type'),
                    '#description' => $this->t('Here are the possible options available. These options are available based on exact field_type matching between source and destination'),
                    '#required' => TRUE,
                    '#options' => $sourceOptions,
                ];
                //Now, we will find if any taxonomy field present in the content type
                $entityManager = \Drupal::service('entity.manager');
                $fields = array_filter(
                    $entityManager->getFieldDefinitions('node', $values['source_content_type']),
                    function ($field_definition) {
                        return
                            $field_definition instanceof FieldConfigInterface;
                    }
                );
                $taxFilter = [];
                $taxFilterMapping = [];
                foreach ($fields as  $field){
                    if($field->getType() == 'entity_reference' && $field->getSettings()['target_type'] == 'taxonomy_term'){
                        //Now, we will show taxonomy as filter
                        $vid = key($field->getSettings()['handler_settings']['target_bundles']);
                        $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
                        $vocabulary = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary')->load($vid);
                        $vocabularyName = $vocabulary->get('name');
                        $taxFilterMapping[$field->get('field_name')] = $vocabularyName;
                        $term_data = [];
                        foreach ($terms as $term) {
                            $term_data[0] = '- Select -';
                            $term_data[$term->tid] = $term->name;
                        }
                        $taxFilter[$vocabularyName] = $term_data;
                    }
                }
                $form['my_ajax_container']['advanced'] = array(
                    '#type' => 'details',
                    '#title' => t('Filters'),
                    '#description' => t('The selected filter will be used to copy the content into destination content type'),
                    '#open' => TRUE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
                );

                $i = 1;
                foreach($taxFilter as $key => $val){
                    $vocabularyMachineName = array_search($key, $taxFilterMapping);
                    $form['my_ajax_container']['advanced']['content_split_tax_filter_' . $i . '_' . $vocabularyMachineName] = array(
                        '#type' => 'select',
                        '#multiple' => TRUE,
                        '#title' => $this->t($key),
                        '#options' => $val,
                    );
                    $i++;
                }
                $form['my_ajax_container']['actions']['#type'] = 'actions';
                $form['my_ajax_container']['actions']['submit'] = array(
                    '#type' => 'submit',
                    '#value' => $this->t('Split'),
                    '#button_type' => 'primary',
                );
            }
            else{
                $form['my_ajax_container']['no_destination'] = array(
                    '#type' => 'markup',
                    '#markup' => '<div>No other content type is similar to <b>'.$values['source_content_type'].'</b>. Make sure you copied the exact machine name of all fields. You can also use this contrib module to clone your content type: '.'<a target="_blank" href="https://www.drupal.org/project/entity_type_clone">Entity Type Clone</a></div>',
                );
            }


        }

        return $form;
    }

    /**
     * The callback function for when the `my_select` element is changed.
     *
     * What this returns will be replace the wrapper provided.
     */
    public function mySelectChange(array $form, FormStateInterface $form_state) {
        // Return the element that will replace the wrapper (we return itself).
        return $form['my_ajax_container'];
    }


    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $filters = [];
        $i = 1;
        $val = $form_state->getValues();
        $query = \Drupal::entityQuery('node')->condition('type', $val['source_content_type']);
        $andGroup = $query->andConditionGroup();
        foreach($val as $formFieldName => $formFieldValue){
            if(strpos($formFieldName, 'content_split_tax_filter_') !== false){
                $filterKey = str_replace('content_split_tax_filter_'.$i.'_', '', $formFieldName);
                if(count($formFieldValue) > 0){
                    $filters[$filterKey] = $formFieldValue;
                    $andGroup->condition($filterKey, $formFieldValue);
                }
                $i++;
            }
        }
        $nids = $query->condition($andGroup)->accessCheck(FALSE)->execute();
        $nodes =  Node::loadMultiple($nids);
        $queue_factory = \Drupal::service('queue');
        /** @var QueueInterface $queue */
        $queue = $queue_factory->get('update_queue');
        foreach($nodes as $node){
            $item = new \stdClass();
            $item->type = $val['destination_content_type'];
            $item->nid = $node->id();
            $queue->createItem($item);
        }
        if (\Drupal::moduleHandler()->moduleExists('queue_ui')) {
            $form_state->setRedirect('queue_ui.overview_form');
        }
        else{
            $this->messenger()->addStatus($this->t('Total ' . count($nids) . ' items are added to queue for processing. You can add '.'<a target="_blank" href="https://www.drupal.org/project/queue_ui">Queue UI</a> module to process it immediately.'));
        }

    }
}

