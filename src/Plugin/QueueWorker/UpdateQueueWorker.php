<?php
/**
 * @file
 * Contains \Drupal\content_split\Plugin\QueueWorker\SyncQueueWorker.
 */

namespace Drupal\content_split\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes tasks for sync module.
 *
 * @QueueWorker(
 *   id = "update_queue",
 *   title = @Translation("Content split update worker"),
 *   cron = {"time" = 90}
 * )
 */
class UpdateQueueWorker extends QueueWorkerBase {

    /**
     * {@inheritdoc}
     */
    public function processItem($item) {
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $updateNode = $node_storage->load($item->nid);
        $updateNode->set('type', $item->type);
        $updateNode->save();
    }
}